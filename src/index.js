import React from 'react';
import {render} from 'react-dom';
import './style.css';

function App() {
  return React.createElement(
    'div',
    {className: 'app'},
    React.createElement('h1', null, 'Hello world!')
  );
}

render(App(), document.getElementById('root'));
