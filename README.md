# FE Summer School

## Основы React
### План лекции
1. Подключение React в рут-элемент
2. Простой Hello World без использования JSX
3. JSX. Встраивание выражений в JSX, условные выражения
4. Транспиляция JXS в JS (см слайд)
5. Компоненты классовый и функциональный (на примере кнопки)
6. Свойства компонента (текст кнопки)
7. Дефолтное значение свойства
8. Children (fate text)
9. Обработчик событий
10. Состояние компонента
11. Зацикливание обновления страницы
12. Методы ЖЦ (вернуться в презентацию)
13. Немножко про архитектуру

## React-router
Устанавливаем пакет react-router-dom
```bash
    npm install --save react-router-dom
```
или
```bash
    yarn add react-router-dom --dev
```

## Что найдете в репозитории
В репозитории несколько веток с примерами кода.
1. master - ветка для работы на лекции, знакомимся вместе с составляющими реакт-приложения.
2. routing-example - пример использования react-router v5.
3. ci_cd-example - пример пайплайна с 3 стейджами и джобами с тестами и линтерами. Деплой статики с помощью surge.
4. gitlab_pages-example - деплой статики на gitlab pages https://galyulia.gitlab.io/react-intro/

## Полезные ссылки
| Тема       | Ссылки                | 
| ------------- |:------------------:| 
| Отрисовка странички     | [Критический путь рендеринга (CRP)](https://developers.google.com/web/fundamentals/performance/critical-rendering-path) | 
| Ключевые алгоритмы реакта| [Как работает React VDOM в картинках](https://maggieappleton.com/react-vdom)  |  
|   | [Объяснение алгоритма согласования верхнеуровнево](https://reactjs.org/docs/reconciliation.html) |  
|   | [React Fiber](https://github.com/acdlite/react-fiber-architecture) |
| React-router| [Хорошая дока с примерами](https://reactrouter.com/web/api) |

## Домашнее задание
Написать мини-приложение на Реакт по [макету](https://www.figma.com/file/Fh7IP7XGqOLcmaHiJq64Nw/Xsolla-Summer-School-2020-Frontend?node-id=0%3A1), но без фильтров по городу и месяцу.  
Использовать те же данные [events.json](https://github.com/xsolla/xsolla-frontend-school-2020/blob/master/events.json)

**Дополнительно:** добавить новую страничку - детализация события. При клике на карточку события открывается новый раздел со всей информацией по мероприятию. Макет этой страницы на ваше усмотрение, можно вывести детели события просто списком. **Ключевое - организовать роутинг.**
